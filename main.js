const menu = [];
const dTime =  10;

class Burger {
  constructor(name, ingredients, time){
    this.name = name || "Burger";
    this.ingredients = ingredients || [
      'Булка',
      'Огурчик',
      'Котлета',
      'Бекон',
      'Рыбная котлета',
      'Соус карри',
      'Кисло-сладкий соус',
      'Помидорка',
      'Маслины',
      'Острый перец',
      'Капуста',
      'Кунжут',
      'Сыр Чеддер',
      'Сыр Виолла',
      'Сыр Гауда',
      'Майонез'
    ];
    this.time = time || dTime;
  }
  addMenu(){
    menu.push({
      name:this.name,
      ingredients:this.ingredients,
      time:this.time,
    })
  }

  _createElem(tag, apTo, text, params) {
    let elem = document.createElement(tag);
    if(params) {
      for(let key in params) {
        if(params.hasOwnProperty(key)) {
          elem.setAttribute(key, params[key])
        }
      }
    }
    if(text) {
      elem.innerText = text
    }
    if(apTo) {
      apTo.appendChild(elem);
    }
    return elem;
  }

  renderMenu(){
    const newDiv = this._createElem('div', document.body, "MENU", {
      'id': 'menu',
      'style':  '2px solid black'
    });
    const ul = this._createElem('ul', newDiv);

    menu.forEach((elem) =>{
      for(let key in elem){
        if(elem.hasOwnProperty(key)) {
          let item = this._createElem("li", ul, `${key} = ${elem[key]}`);
        }
      }
      this._createElem("hr", this._createElem("li", ul));
    });
  }
}


let standardBurger = new Burger("standardBurger");
standardBurger.addMenu();

class newBurger extends Burger {
  constructor(name, ingredients, time){
    super(name,ingredients,time);
  }
  addIngredients(ingr){
    if(Array.isArray(ingr)) {
      this.ingredients = this.ingredients.concat(ingr)
    } else {
      this.ingredients.push(ingr)
    }
  }
}

const cheeseBurger = new newBurger("CheeseBurger");
cheeseBurger.addIngredients("Двойной сыр");
cheeseBurger.addMenu();

const hamburger = new newBurger("Hamburger");
hamburger.addIngredients("Помидорка");
hamburger.addMenu();
hamburger.renderMenu();

const order =[];

class Order {
  constructor(name,condition,value){
    this.name = name;
    this.condition = condition;
    this.value = value;
  }
  search(menu){
    const self = this;
    menu.map((obj) =>{
      if(obj.name === this.name){
        console.log(`${obj.name}, будет готов через ${obj.time} мин.`);
        order.push(obj);
      } else if(this.condition) {
        if(obj.ingredients.indexOf(this.condition) !== -1)
          console.log(`${obj.name} с ${this.value} будет готов через ${obj.time} мин.`);
          order.push(obj);
      }
      else if(this.value) {
        if(obj.ingredients.indexOf(this.value) !== -1)
          console.log(`${obj.name} без ${this.value} будет готов через ${obj.time} мин.`);
          order.push(obj);
      }
    })
  }
}

const orderHamburger = new Order("", "","Помидорка");
orderHamburger.search(menu);

const orderCheeseBurger = new Order("CheeseBurger");
orderCheeseBurger.search(menu);

const orderChickenBurger = new Order("","Сырок");
orderChickenBurger.search(menu);